from django import forms
from todos.models import TodoList,TodoItem


class TodoForm(forms.ModelForm):

    class Meta:
        model = TodoList
        fields = [
            "name",
        ]
class TodoItemForm(forms.ModelForm):
    due_date = forms.DateTimeField(widget=forms.DateTimeInput(attrs={'type': 'datetime-local'}))
    class Meta:
        model = TodoItem
        fields = ['task', 'due_date', 'is_completed', 'list']

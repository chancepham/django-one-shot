from django.db import models

class TodoList(models.Model):
    name = models.CharField(max_length=100)  # String field with a maximum length of 100 characters
    created_on = models.DateTimeField(auto_now_add=True)  # Automatically set to the time the instance is created

    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=100)  # String field for the task description
    due_date = models.DateTimeField(null=True, blank=True)  # Optional datetime field for the due date
    is_completed = models.BooleanField(default=False)  # Boolean field, defaults to False
    list = models.ForeignKey(
        'TodoList',  # This references the TodoList model
        on_delete=models.CASCADE,  # Ensures deletion of todo items if the list is deleted
        related_name='items'  # Allows access to todo items from the TodoList model via .items
    )

    def __str__(self):
        return self.task

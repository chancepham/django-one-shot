from django.urls import path
from .views import todo_list_list, todo_list_detail, TodoListCreate, todo_list_edit, delete_todolist,create_todoitem,edit_todoitem,search_todoitem

urlpatterns = [
    path('', todo_list_list, name='todo_list_list'),
    path('<int:id>/', todo_list_detail, name='todo_list_detail'),
    path('create/', TodoListCreate, name='todo_list_create'),
    path('<int:id>/edit/', todo_list_edit, name='todo_list_update'),
    path('items/<int:id>/edit/', edit_todoitem, name='todo_item_update'),
    path('<int:id>/delete/', delete_todolist, name='todo_list_delete'),
    path('items/create/', create_todoitem, name='todo_item_create'),
    path('items/search/', search_todoitem, name='todo_item_search'),
]

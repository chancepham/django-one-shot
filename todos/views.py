from django.shortcuts import render, get_object_or_404, redirect
from django.urls import reverse
from .models import TodoList, TodoItem
from .forms import TodoForm, TodoItemForm

def todo_list_list(request):
    lists = TodoList.objects.all()  # Get all TodoList instances
    return render(request, 'todos/todos.html', {'lists': lists})
def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    items = TodoItem.objects.filter(list=todo_list)
    return render(request, 'todos/detail.html', {'todo_list': todo_list, 'items': items})
def create_todoitem(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect('todo_list_detail', id=todo_item.list.id)
    else:
        form = TodoItemForm()
    return render(request, 'todos/create_item.html', {'form': form})
def TodoListCreate(request):
    if request.method == 'POST':
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect(reverse('todo_list_detail', args=[todo_list.id]))
    else:
        form = TodoForm()  # An empty form
    return render(request, 'todos/form.html', {'form': form})
def todo_list_edit(request, id):
    todo = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoForm(instance=todo)

    context = {
        "todo_object": todo,
        "form": form,
    }
    return render(request, "todos/edit.html", context)
def edit_todoitem(request, id):
    todo_item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todo_item)
        if form.is_valid():
            form.save()
            return redirect('todo_list_detail', id=todo_item.list.id)
    else:
        form = TodoItemForm(instance=todo_item)
    return render(request, 'todos/edit_item.html', {'form': form, 'lists': TodoList.objects.all()})
def delete_todolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect('todo_list_list')
    return render(request, 'todos/delete.html', {'todolist': todolist})
def search_todoitem(request):
    query = request.GET.get('q')
    results = []
    if query:
        results = TodoItem.objects.filter(task__icontains=query)
    return render(request, 'todos/search.html', {'results': results, 'query': query})
